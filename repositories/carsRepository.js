const { Cars } = require("../models");

class CarsRepository {
  static async create({
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    type,
    year,
    options,
    specs,
    availableAt,
    isWithDriver,
    createdBy,
    updatedBy,
  }) {
    const createdCar = Cars.create({
      plate,
      manufacture,
      model,
      image,
      rentPerDay,
      capacity,
      description,
      transmission,
      type,
      year,
      options,
      specs,
      availableAt,
      isWithDriver,
      createdBy,
      updatedBy,
    });

    return createdCar;
  }

  static async getAll() {
    const getAll = await Cars.findAll();

    return getAll;
  }

  static async update({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    type,
    year,
    options,
    specs,
    availableAt,
    isWithDriver,
    updatedBy,
  }) {
    const updatedCar = Cars.update(
      {
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        availableAt,
        isWithDriver,
        updatedBy,
      },
      { where: { id } }
    );

    return updatedCar;
  }

  static async delete({ id }) {
    const deletedCar = await Cars.destroy({
      where: {
        id,
      },
    });

    return deletedCar;
  }

  static async getAllCars({ availableAt, isWithDriver }) {
    if (isWithDriver && availableAt) {
      const filteredCars = await Cars.findAll({
        where: {
          isWithDriver,
          availableAt,
        },
      });

      return filteredCars;
    }

    return Cars;
  }
}

module.exports = CarsRepository;
